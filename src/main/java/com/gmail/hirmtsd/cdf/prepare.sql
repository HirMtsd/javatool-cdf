-- CheckDuplicateFile用SQLite DBファイル作成用
-- 2022/07/08(FRI)
CREATE TABLE T_HASH (
  NUM INTEGER, -- 個数
  LVL INTEGER, -- 階層
  DIR_NAME TEXT, -- ディレクトリ名
  FILE_NAME TEXT, -- ファイル名
  SIZE INTEGER, -- サイズ
  LAST_MOD TEXT, -- 最終更新日
  MD5 TEXT, --
  SHA1 TEXT, --
  SHA256 TEXT, --
  SHA512 TEXT, --
  SHA3_512 TEXT --
);
CREATE INDEX IDX_T_HASH ON T_HASH(MD5);
