/**
 * JavaTool : 重複ファイルチェック
 * 2022/06/28(TUE)
 */

package com.gmail.hirmtsd.cdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * 重複ファイルチェック
 *
 * @author @HirMtsd
 */
public class CheckDuplicateFile {

  /**
   * バッチ起動用
   *
   * @param args コマンドライン引数
   */
  public static void main(String[] args) {
    // 対象ディレクトリ
    String trgDir = "C:\\";
    // 結果保持用SQLiteDB
    String sqliteDbOrg = "hash_org.db";
    // 重複結果出力ファイル
    String outDupFile = "C:\\duplicateFile.txt";
    
    CheckDuplicateFile cdf = new CheckDuplicateFile();
    long all = cdf.exec(trgDir, sqliteDbOrg, outDupFile);
    System.out.println("all=" + all);
  }

  /**
   * 実行メソッド
   *
   * @param trgDir 対象ディレクトリ
   * @param sqliteDbOrg 結果保持用SQLiteDB
   * @param outDupFile 重複結果出力ファイル
   * @return 総処理件数
   */
  public long exec(final String trgDir, final String sqliteDbOrg, final String outDupFile) {
    // 時刻計測
    LocalDateTime ldt = LocalDateTime.now();
    System.out.println("start=" + ldt);
    
    // SQLiteDBファイル名の生成
    
    String sqliteDb = "hash_" + ldt.format(DateTimeFormatter.ISO_LOCAL_DATE) + ".db";
    // SQLiteDBファイルのコピー
    try {
      Files.copy(Paths.get(sqliteDbOrg), Paths.get(sqliteDb));
    } catch (IOException e1) {
      System.out.println("ファイルコピーに失敗[" + sqliteDbOrg + "],[" + sqliteDb + "]");
      e1.printStackTrace();
      System.exit(1);
    }

    // SQLiteDBファイルのオープン
    Connection con = null;
    try {
      con = DriverManager.getConnection("jdbc:sqlite:" + sqliteDb);
    } catch (SQLException e) {
      System.out.println("次のファイルが見つかりません" + sqliteDb);
      e.printStackTrace();
      System.exit(1);
    }
    
    // 指定ディレクトリのFileクラスを取得
    File fd = new File(trgDir);
    
    // 総処理券数
    long all = 0;
    
    // ファイルのチェック
    all += checkDir(con, fd, 0);
    
    // チェック結果
    long dup = searchDuplicate(con);
    
    // 時刻計測
    ldt = LocalDateTime.now();
    System.out.println("end=" + ldt);

    return all;
  }

  /**
   * ディレクトリのチェック
   *
   * @param con DB接続
   * @param fd ディレクトリ
   * @param lvl 階層
   * @return 処理件数
   */
  public long checkDir(final Connection con, final File fd, long lvl) {
    long ret = 0;
    
    // ディレクトリの要素が空の場合
    if (fd.listFiles() == null) {
      return ret;
    }
    
    // ディレクトリ名
    String dir = fd.getPath();
    System.out.println("ret:" + ret + " lvl:" + lvl + " dir:" + dir);
    
    // 
    for (File c : fd.listFiles()) {
      // ファイルの場合
      if (c.isFile()) {
        // ファイル名
        String name = c.getName();
        long size = c.length();
        // 更新日時EpochTimeを変換
        Instant inst = Instant.ofEpochMilli(c.lastModified());
        LocalDateTime ldt = LocalDateTime.ofInstant(inst, ZoneId.systemDefault());
        
        // ファイルがRead可能ならハッシュを求める
        String hash = "N/A";
        if (c.canRead()) {
          try {
            hash = DigestUtils.md5Hex(new FileInputStream(c));
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
        
        // DBへINSERT
        String insert = "INSERT INTO T_HASH("
            + "NUM, LVL, DIR_NAME, FILE_NAME, SIZE, LAST_MOD, MD5, SHA1, SHA256, SHA512, SHA3_512)"
            + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement pstm;
        try {
          pstm = con.prepareStatement(insert);
          pstm.setLong(1, ret);
          pstm.setLong(2, lvl);
          pstm.setString(3, dir);
          pstm.setString(4, name);
          pstm.setLong(5, size);
          pstm.setString(6, ldt.toString());
          pstm.setString(7, hash);
          pstm.setString(8, "");
          pstm.setString(9, "");
          pstm.setString(10, "");
          pstm.setString(11, "");
          pstm.executeUpdate();
        } catch (SQLException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        
        // 加算
        ret++;
      } else if (c.isDirectory()) {
        // ディレクトリの場合
        
        // このメソッドを再度呼び出す
        ret += checkDir(con, c, lvl + 1);
      }
    }
    
    //
    return ret;
  }

  /**
   * 重複検索
   *
   * @param con DB接続
   * @return 処理件数
   */
  public long searchDuplicate(final Connection con) {
    // TODO Auto-generated method stub
    return 0;
  }

}
